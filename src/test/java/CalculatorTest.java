import nl.sogeti.Calculator;
import org.junit.Test;

import static org.junit.Assert.*;

public class CalculatorTest {

    private Calculator calculator = new Calculator();

    @Test
    public void testAdd(){
        assertEquals("Test that the values 1 and 2 returns 3.0", 3.0f, calculator.add(1, 2), 0);
    }

    @Test
    public void testDivide(){
        assertEquals("Test that the values 1 and 2 returns 0.5", 0.5f, calculator.divide(1, 2), 0);
    }
}
