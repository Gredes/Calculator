package nl.sogeti;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/calculate")
public class Calculator extends HttpServlet {

    /**
     * Adds the 2 values and returns this value
     */
    public float add(float value1, float value2){
        return value1 + value2;
    }

    /**
     * Divides the 2 values and returns this value
     */
    public float divide(float value1, float value2){
        return value1 / value2;
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        // Retrieve the values from the request
        float value1 = Float.parseFloat(request.getParameter("value1"));
        float value2 = Float.parseFloat(request.getParameter("value2"));
        String operation = request.getParameter("operation");

        // Determine the operation we should perform
        float result = 0;
        switch (operation){
            case "Add":
                result = add(value1, value2);
                break;
            case "Divide":
                result = divide(value1, value2);
                break;
        }

        // Set the result on the request
        request.setAttribute("result", result);

        // Return to the index
        RequestDispatcher view = request.getRequestDispatcher("/index.jsp");
        view.forward(request, response);
    }
}
