<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>

<form method="post" action="${pageContext.servletContext.contextPath}/calculate">

    First value:
    <br />
    <input type="number" name="value1"/>
    <br />

    Second value:
    <br />
    <input type="number" name="value2"/>
    <br />

    Operation:
    <br />
    <select name="operation">
        <option>Add</option>
        <option>Divide</option>
    </select>

    <input type="submit" name="submit" value="submit" /></p>

    <br />
    Result: ${result}

</form>

</body>
</html>
